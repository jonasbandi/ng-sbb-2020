import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greeter',
  template: `
    <p>Hello {{ name }}</p>
    <input [(ngModel)]="name" />

    <div>
      <button (click)="toggleContent()">Toggle Content</button>
      <button (click)="addName()">Add name</button>
      <br />
      <div *ngIf="showContent">
        More content
      </div>
      <ul>
        <li *ngFor="let name of names">{{ name }} <button>X</button></li>
      </ul>
    </div>
  `,
  styleUrls: ['./greeter.component.css']
})
export class GreeterComponent implements OnInit {
  name = '';
  showContent = false;

  names = ['JOnas', 'Bandi', 'Gugus'];

  constructor() {}

  ngOnInit(): void {}




  toggleContent() {
    this.showContent = !this.showContent;
  }

  addName() {
    this.names.push(new Date().toISOString());
  }
}
