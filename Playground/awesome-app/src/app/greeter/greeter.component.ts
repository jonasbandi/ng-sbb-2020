import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greeter',
  template: `
    <h2>Greeter Component</h2>
    <h3>Hello, my name is {{ name }}</h3>

    <input [(ngModel)]="name" />
  `,
  styleUrls: ['./greeter.component.css']
})
export class GreeterComponent implements OnInit {
  name = 'Jonas';
  isDisabled = false;

  constructor() {
    setInterval(() => {
      this.name = new Date().toISOString();
    }, 5000);
  }

  ngOnInit(): void {}

  updateName(newName: string) {
    this.name = newName;
  }
}
