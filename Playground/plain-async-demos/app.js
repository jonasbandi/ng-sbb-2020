console.log('Running example !!!!!!');

const {
  of,
  from,
  throwError,
  fromEvent,
  interval,
  merge,
  zip,
  forkJoin
} = rxjs;
const {
  filter,
  map,
  delay,
  mergeMap,
  flatMap,
  switchMap,
  concatMap,
  tap,
  scan,
  take,
  skip
} = rxjs.operators;
const { ajax } = rxjs.ajax;

let myObservable = interval(1000);
// let myObservable = ajax('https://swapi.co/api/people/1/');

myObservable
  .pipe(
    map(v => v + 1),
    map(v => `https://swapi.co/api/people/${v}/`),
    mergeMap(v => ajax(v)),
    tap(v => console.log(v)),
    map(r => r.response.name)
  )
  .subscribe(
    v => {
      console.log('Value received', v);
      document.getElementById('content').innerText = v;
    },
    e => console.log('Error', e),
    () => console.log('Completed!')
  );

//
// axios
//   .get('https://swapi.co/api/people/1/')
//   .then(response => {
//     console.log('Luke has arrived', response.data);
//     return axios.get(response.data.films[0]);
//   })
//   .then(filmResponse => {
//     console.log('Film has arrived', filmResponse.data);
//     let x = filmResponse.gugus.hekll;
//     return axios.get(filmResponse.data.planets[0]);
//   })
//   .then(planetResponse => {
//     console.log('Film has arrived', planetResponse.data);
//   })
//   .then(() => {
//     console.log('REALLY finished!');
//   })
//   .catch(e => {
//     console.log('ERRRORR', e);
//   });
// console.log('Finished!');

// $.get('https://swapi.co/api/people/1/', response => {
//   console.log('Luke has arrived', response);
//   $.get(response.films[0], filmResponse => {
//     console.log('Film:', filmResponse);
//     $.get(filmResponse.planets[0], planetResonse => {
//       console.log('Planet', planetResonse);
//     });
//   });
// });

/// API:
// http://localhost:3456/todos
// https://swapi.co/api/people/1/

// // importing Rx
// const {of, from, throwError, fromEvent, interval, merge, zip, forkJoin} = rxjs;
// const {filter, map, delay, mergeMap, flatMap, switchMap, concatMap, tap, scan, take, skip} = rxjs.operators;
// const {ajax} = rxjs.ajax;

// CALLBACKS
// $.get('https://swapi.co/api/people/1/', (response) => {
//   console.log('Person 1', response);
//   $.get(response.films[0], (response) => {
//     console.log('Film 1', response);
//     $.get(response.planets[0], (response) => {
//       console.log('Planet 1', response);
//     })
//   })
// });

// PROMISE
// axios.get('https://swapi.co/api/people/1/')
//   .then((response) => {
//     console.log('Person 1', response.data);
//     return axios.get(response.data.films[0])
//   })
//   .then((response) => {
//     console.log('Film 1', response.data);
//     return axios.get(response.data.planets[0])
//   })
//   .then((response) => {
//     console.log('Planet 1', response.data);
//   });

// ASYNC/AWAIT
// async function doit() {
//   const personResponse = await axios.get('https://swapi.co/api/people/1/');
//   console.log('Person 1', personResponse.data);
//
//   const filmResponse = await axios.get(personResponse.data.films[0]);
//   console.log('Film 1', filmResponse.data);
//
//   const planetResponse = await axios.get(filmResponse.data.planets[0]);
//   console.log('Planet 1', planetResponse.data);
// }
//
// doit();

// OBSERVABLES/RXJS
// ajax.get('https://swapi.co/api/people/1/')
//   .pipe(
//     tap(result => console.log('Person 1', result.response)),
//     switchMap(result => ajax.get(result.response.films[0])),
//     tap(result => console.log('Film 1', result.response)),
//     switchMap(result => ajax.get(result.response.planets[0])),
//     tap(result => console.log('Planet 1', result.response))
//   )
//   .subscribe();
