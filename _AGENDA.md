# Agenda Angular Course
Day 1:

08:30 

- Intro - (Poll: http://etc.ch/3Jbg ) 
- Exercise 1: Create an Angular App
- Understanding an Angular Project
- Angular Components
- Exercise 2: Create your First Component
- Client Side Routing
- Exercise 3: Create an Angular App With Routing
- Templating & Directives

12:00

LUNCH

13:00

- Exercise 4.1: ToDo App: Single Component
- Component Architecture
- Exercise 4.2: ToDo App: Multiple Components

- Exercise 4.3: ToDo App: Routing
- (Reserve) More UI Constructs

17:00



Day 2:

08:30 

- Discussion Exercise 4.2 & 4.3
- More UI Contructs (Demos)
- Angular Forms
- Exercise 5: ToDo App with Form
- Sidetrack: Async in JavaScript: Callbacks, Promises, Observables

12:00

LUNCH

13:00

- Exercise: Backend Access
- More Routing
- Change Detection & State Management / Intro to NgRx

17:00