import { Component, ElementRef, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { styleDef } from './js/style';

@Component({
  template: `
    <h3 class="css-global"> global style via CSS (angular.json) </h3>
    <h3 class="scss-global"> global style via SCSS  </h3>
    <h3 class="scss-global-variable"> global style via SCSS (using variable)  </h3>
    <h3 class="css-direct"> component style via CSS (direct) </h3>
    <h3 class="css-import"> component style via CSS (with import) </h3>
    <h3 class="css-variable-global"> component style via CSS (with global css variable) </h3>
    <h3 class="css-variable-local"> component style via CSS (with local css variable) </h3>
    <h3 class="css-import"> component style via CSS (with import) </h3>
    <h3 class="scss-direct"> component style via SCSS (direct) </h3>
    <h3 class="scss-import"> component style via SCSS (with import) </h3>
    <h3 class="scss-import-variable"> component style via SCSS (with import variable) </h3>
    <h3 class="scss-mixin"> component style via SCSS (with mixin) </h3>
    <h3 class="inline-direct"> component style inline (direct) </h3>
    <h3 class="inline-string"> component style inline (string) </h3>
    <h3 [ngStyle]="styles"> component style with ngStyle </h3>
    <h3 [ngClass]="classes"> component style with ngClass </h3>
  `,
  styleUrls: [
    './css/style.css',
    './css/ng-classes.css',
    './scss/style.scss',
  ],
  styles: [`
    .inline-direct {
        color: lime;
    }
  `,
    styleDef
  ]
})
export class StylingOptionsDemoComponent implements OnInit {

  styles = {color: 'darkviolet'};

  classes = {ngClassV1: false, ngClassV2: true};
  // classes = ['ngClassV2'];

  constructor(private element: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {

    // DEMO: Setting css variables programmatically
    // let color = 'blue';
    // setInterval(() => {
    //   color = color === 'blue' ? 'red' : 'blue';
    //   this.renderer.setAttribute(this.element.nativeElement, 'style', `--local-color: ${color}`);
    //   document.documentElement.style.setProperty('--global-color', `${color}`);
    // }, 1000);

  }

}
