import { ChangeDetectionStrategy, ChangeDetectorRef, Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {delay1} from './util';

@Component({
  selector: 'aw-change-nested-child',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
    <h3>Nested Child {{value}}</h3>
    <div>{{message}}</div>
    <ng-content></ng-content>
  `,
  styles: [`:host {display: block; background-color: lightblue; padding: 5px}`]
})
export class ChangeTrackingNestedChildComponent  {

  private _message = 'Hello World';
  @Input() value = 0;

  get message() {
    console.log(`Nested Child ${this.value} -> Message Getter`);
    // delay1(1000);
    return this._message;
  }

  // constructor(private changeDetectionRef: ChangeDetectorRef) {
  //   setInterval(() => {
  //     this._message = new Date().toISOString();
  //     console.log('Nested Child ${this.value} -> updating value');
  //     // changeDetectionRef.markForCheck();
  //   }, 1000);
  // }

}
